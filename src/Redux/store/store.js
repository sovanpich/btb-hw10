import reducers from '../reducers/rootReducer'
import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'



const store = createStore(reducers, {}, applyMiddleware(thunk,logger))

export default store