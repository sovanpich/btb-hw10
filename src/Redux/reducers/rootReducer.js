import { combineReducers } from 'redux';
import article_reducer from './article_reducer'

const reducers = combineReducers({
    article_reducer
});

export default reducers;