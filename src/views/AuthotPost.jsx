import React, { useState, useEffect } from 'react'
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { useParams } from "react-router";
import { fetchAuthorById, postAuthor, uploadImage,updateAuthorById } from '../services/author_service';
import ViewAuthor from './ViewAuthor';

function AuthotPost() {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png')
    const [imageFile, setImageFile] = useState(null)
  
    const onAdd = async (e) => {
        e.preventDefault()
        let author = {
            name,email
        }


        if (imageFile) {
            let url = await uploadImage(imageFile)
            author.image = url
        }
        postAuthor(author).then(message => alert(message))
    }

    const onUpdate = async (e) => {
        e.preventDefault()
        let author= {
            name,email
        }
        if (imageFile) {
            let url = await uploadImage(imageFile)
            author.image = url
        }
        updateAuthorById(author).then(message => alert(message))
    }

    return (
        <Container>
            <h1 className="my-2">Author</h1>
            <Row>
                <Col md={8}>
                    <Form>
                        <Form.Group controlId="author">
                            <Form.Label>Author</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Author"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                            />
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>
                        <Form.Group controlId="email">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />

                        </Form.Group>
                        <Button
                            variant="primary"
                            type="submit"
                            onClick={()=>onAdd()}
                        >
                            Add
                        </Button>
                    </Form>
                </Col>
                <Col md={4}>
                    <img className="w-100" src={imageURL} />
                    <Form>
                        <Form.Group>
                            <Form.File
                                id="img"
                                label="Choose Image"

                            />
                        </Form.Group>
                    </Form>
                </Col>
            </Row>
            <Row>
                <Col>
                    <ViewAuthor/>
                </Col>
            </Row>

        </Container>
    )
}

export default AuthotPost
