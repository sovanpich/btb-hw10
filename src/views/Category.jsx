import React, { useState,useParams,useEffect } from "react";
import { Container, Form, Table, Button } from "react-bootstrap";
import { addCategory, fetchCategory } from "../services/category_service";

function Category() {
  const [name, setName] = useState([])
  const [categoryId, setCategoryId] = useState(null)
  useEffect(async() => {
    let category = await addCategory()
    setName(category)
    },[])
     
  const onAdd =async (e) => {
    
    e.preventDefault()
    addCategory(name).then(message=>alert(message))
  }

  return (
    <Container>
      <h1 className="my-3">Category</h1>
      <Form>
        <Form.Group controlId="category">
          <Form.Label>Category Name</Form.Label>
          <Form.Control type="text" placeholder="Category Name" value={name} onChange={(e) => setName(e.target.value)} />
          <br />
          <Button variant="secondary" onClick={onAdd}>Add</Button>
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>
      </Form>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>{name}</td>
            <td>
              <Button
                size="sm"
                variant="warning"
              >
                Edit
              </Button>{" "}
              <Button
                size="sm"
                variant="danger"
              >
                Delete
              </Button>
            </td>
          </tr>
        </tbody>
      </Table>
    </Container>
  );
};

export default Category;
