import React from 'react'

function ArticleCard() {
    return (
        <Col md={3} key={article._id}>
            <Card className="my-2">
                <Card.Img
                    variant="top"
                    style={{ objectFit: "cover", height: "150px" }}
                    src={article.image ? article.image : "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"}
                />
                <Card.Body>
                    <Card.Title>{article.title}</Card.Title>
                    <Card.Text className="text-line-3">
                        {article.description}
                    </Card.Text>
                    <Button
                        size="sm"
                        variant="primary"
                    // onClick={()=>
                    //   history.push('/article/'+article._id)
                    // }
                    >
                        View
                    </Button>{" "}
                    <Button
                        size="sm"
                        variant="warning"
                    // onClick={()=>{
                    //   history.push('/update/article/'+article._id)
                    // }}
                    >
                        Edit
                    </Button>{" "}
                    {/* onClick={()=>onDelete(article._id)} */}
                    <Button size="sm" variant="danger" >
                        Delete
                    </Button>
                </Card.Body>
            </Card>
        </Col>
    )
}

export default ArticleCard
